import React, { Component } from 'react';
import logo from './logo.svg';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faIgloo } from '@fortawesome/free-solid-svg-icons';
import Header from './components/Header.js';
import Sidebar from './components/Sidebar.js';
import Posts from './components/Posts.js';
import Login from './components/Login.js';
import Home from './components/Home.js';
import samplePosts from './components/sample-posts.js';
import './App.css';
library.add(faIgloo);

class App extends Component {
  constructor() {
    super();
    this.handleData = this.handleData.bind(this);
    this.state = {
      loaded: false,
      isLogged: false,
      id: '',
      posts : {}
    }
  }

  loadSamplePosts() {
    this.setState({ posts: samplePosts });
  }

  async handleData(user,password) {
    console.log(user+password);
    const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({user:user, pass:password})
    });

    const body = await response.json();

    this.setState({ id: body.id })
  }

  render() {
    const {loaded, isLogged, id} = this.state;

    if (loaded === false && id !== '') { this.setState({ loaded: true, isLogged: true }) }

      if (isLogged) {
        return (
          <div className="App">
            <Header />
            <Sidebar />
            <Posts posts={this.state.posts} />
          </div>
        )
      } else {
        return (
          <div className="App">
            <Login handlerFromParent={this.handleData} />
          </div>
        )
      }
  }
}

export default App;
