import React, { Component } from 'react';
import '../App.css';
import Content from './Content';

class Posts extends Component {

  render() {
    return (
      <div className="Content">
      	<Content />
      </div>
    );
  }
}

export default Posts;
