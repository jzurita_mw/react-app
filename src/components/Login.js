import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Login extends Component {
	constructor() {
		super();
		this.userhandleChange = this.userhandleChange.bind(this);
		this.passwordhandleChange = this.passwordhandleChange.bind(this);
    	this.submitHandler = this.submitHandler.bind(this);
		this.state = {
	      user: '',
	      password: ''
	    };
	}

	submitHandler(evt) {
	    evt.preventDefault();
	    // pass the input field value to the event handler passed
	    // as a prop by the parent (App)
	    this.props.handlerFromParent(this.state.user,this.state.password);
	}

	userhandleChange(event) {
	    this.setState({
	      user: event.target.value
	    });
	}

	passwordhandleChange(event) {
	    this.setState({
	      password: event.target.value
	    });
	}

	render() {
	return (
	<div className="App-header">
	    <div className="login">
	    	<h1>Login</h1>
			<FontAwesomeIcon icon="igloo" />
			<form onSubmit={this.submitHandler}>
		        <label htmlFor="username">Usuario</label>
		        <input id="username" name="username" type="text" value={this.state.user} onChange={this.userhandleChange}/>
		        <br />
		        <label htmlFor="email">Clave</label>
		        <input id="password" name="password" type="password" value={this.state.password} onChange={this.passwordhandleChange} />
		        <br />
		        <button>Loguear</button>
		      </form>
		</div>
	</div>
	);
	}
}

export default Login;